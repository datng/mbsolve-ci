# Purpose
This repository contains various Dockerfiles. These are used to generate images for the CI pipeline of mbsolve.

# Images
- centos: Based on CentOS 7
- opensuse/leap: Based on OpenSUSE Leap 15.1
- ubuntu: Based on Ubuntu 18.04